create database sriraksha_books;

use sriraksha_books;

create table Books (id int primary key,title varchar(50),author varchar(50));

insert into Books values(1, 'Believe in Yourself', 'Joseph murphy');
insert into books values(2, 'Great Expectations', 'Charles Dickens');
insert into books values(3, 'States', 'Chetan Bhagat');
insert into books values(4, 'Tell me a strory', 'ravinder singh');
insert into books values(5, 'The Lord of the Rings', 'J.R.R. TOLKIEN');
insert into books values(6, 'THE SECRET OF THE OLD CLOCK', 'CAROLYN KEENE');
insert into books values(7, 'THE DARK ROAD', 'MA JIAN');
insert into books values(8, 'Tale Of Two Cities', 'CHARLES DICKENS');
insert into books values(9, 'The MONK who sold His FERRARI', 'ROBIN SHARMA');
insert into books values(10, 'Five point someone', 'Chetan Bhagat');

create table Login(email varchar(20) primary key,password varchar(20) not null);

create table LikedBooks(id int,title varchar(50),author varchar(50),user varchar(20));

create table readlaterbooks(id int,title varchar(50),author varchar(50),user varchar(20));
