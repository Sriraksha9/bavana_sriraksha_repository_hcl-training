package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.User;
import com.service.UsersService;

@RestController
@RequestMapping(value="user")
public class UsersController {
	
	@Autowired
	UsersService usersService;
	
	@PostMapping(value="store",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody User user) {
		return usersService.storeUserDetails(user);
		
	}
	
	@PatchMapping(value="updatePassword")
	public String updateUserPassword(@RequestBody User user) {
		return usersService.updateUserDetails(user);
	}
	
	@GetMapping(value="AllDetails")
	public List<User> getAllDetailsOfUsers(){
		return usersService.getAllUsersDetails();
	}
	
	@DeleteMapping(value="deleteByUser_login/{User_login}")
	public String deleteUserDetails(@PathVariable("User_login") String User_login) {
		return usersService.deleteUserDetails(User_login);
	}
}
