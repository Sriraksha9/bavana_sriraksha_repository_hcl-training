package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity

public class User {
	
	@Id
	
	private String User_login;
	private String User_password;
	
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(String user_login, String user_password) {
		super();
		User_login = user_login;
		User_password = user_password;
	}
	
	public String getUser_login() {
		return User_login;
	}
	public void setUser_login(String user_login) {
		User_login = user_login;
	}
	public String getUser_password() {
		return User_password;
	}
	public void setUser_password(String user_password) {
		User_password = user_password;
	}
	
	@Override
	public String toString() {
		return "Users [User_login=" + User_login + ", User_password=" + User_password + "]";
	}
	
}
