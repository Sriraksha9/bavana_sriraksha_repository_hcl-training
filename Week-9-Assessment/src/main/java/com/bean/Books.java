package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity

public class Books {
	
	@Id
	
	private int Id;
	private String Title;
	private String Author;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getAuthor() {
		return Author;
	}
	public void setAuthor(String author) {
		Author = author;
	}
	
	@Override
	public String toString() {
		return "Books [Id=" + Id + ", Title=" + Title + ", Author=" + Author + "]";
	}
	
}
