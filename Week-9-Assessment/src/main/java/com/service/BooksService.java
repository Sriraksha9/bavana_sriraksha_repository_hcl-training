package com.service;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Books;
import com.dao.BooksDao;

@Service

public class BooksService {
	
	@Autowired
	
	BooksDao booksDao;
	
	public List<Books> getAllBooks(){
		return booksDao.findAll();
		
	}
	
	public Optional<Books> getBookById(int bid ) {
		return booksDao.findById(bid);
	}
	
	public String storeBook(Books book) {

		if (booksDao.existsById(book.getId())) {
			return "Book id should be unique";
		} else {
			booksDao.save(book);
			return "Book stored successfully";
		}
	}
	
	public String updateBookName(Books book) {
		if (!booksDao.existsById(book.getId())) {
			return "No Book Found";
		} else {
			Books b = booksDao.getById(book.getId());
			b.setTitle(book.getTitle());
			booksDao.saveAndFlush(b);
			return "Book updated successfully";
		}
	}
	
	public String deleteBook(int Id) {
		if (!booksDao.existsById(Id)) {
			return "Book  details not present";
		} else {
			booksDao.deleteById(Id);
			return "Book deleted successfully";
		}
	}
}
