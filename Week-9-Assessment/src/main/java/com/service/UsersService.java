package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.User;
import com.dao.UsersDao;

@Service

public class UsersService {
	
	@Autowired
	UsersDao usersDao;
	
	//Store User Details
	public String storeUserDetails(User user) {
		if(!usersDao.existsById(user.getUser_login())) {
			usersDao.save(user);
			return "user your details saved sucessfully";
		}else {
			return "your details are already present";
		}
	}
	
	//Update User Details ByUsername
	public String updateUserDetails(User user) {
		if(usersDao.existsById(user.getUser_login())) {
			User u=usersDao.getById(user.getUser_login());
			if(u.getUser_password().equals(user.getUser_password())) {
				return "You are providing the old input";
			}else {
				u.setUser_password(user.getUser_password());
				usersDao.saveAndFlush(u);
				return "your details are updated sucessfully";
			}
		}else {
			return "your details are not present";			
		}						
	}
	
	//ToSee All Users Information
	public List<User> getAllUsersDetails() {
		return usersDao.findAll();
	}

	//To delete a userBy Username
	public String deleteUserDetails(String User_login) {
		if(!usersDao.existsById(User_login)) {
			return "your details are not present";
		}else {
			usersDao.deleteById(User_login);
			return "user Details Deleted sucessfully";
		}
	}
}
