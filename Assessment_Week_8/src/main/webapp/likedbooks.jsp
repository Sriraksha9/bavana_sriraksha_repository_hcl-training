<%@page import="com.bean.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<h4 align="center">Your liked books are here</h4>
<a href="Welcome.jsp">Click here to go back to dashboard</a>
<body style="background-color:aqua; ;">
<div align="center">
<table border="1">
	<tr>
			<th>Id</th>
			<th>TITLE</th>
			<th>AUTHOR</th>
			
	</tr>
<% 
	Object user=session.getAttribute("user");
	if(user!=null){
	out.println("WELCOME TO DASHBORAD "+user);
	}
	Object obj = session.getAttribute("obj2");
	List<Books> listOfBooks = (List<Books>)obj;
	Iterator<Books> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Books book  = ii.next();
		%>
		<tr>
			<td><%=book.getId() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getAuthor() %></td>
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>