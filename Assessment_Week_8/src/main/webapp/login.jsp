<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="background-color:aqua;">
<h2 align="center">WELCOME TO LOGIN PAGE</h2>
<hr>
<a href="Register.spring">Click here to register if you are visiting first time</a>
<hr><br>
<%
Object logmsg=request.getAttribute("logmsg");
if(logmsg!=null){
out.println(logmsg);
}
%>
<div align="center">
<h3>Please submit your details to login into the application</h3>
<form action="login.spring" method="get">
<label>Email</label>
<input type="email" name="email" required="required"><br>
<label>Password</label>
<input type="password" name="pass" required="required"><br>
<input type="submit" value="Submit"><br>
<input type="reset" value="Clear">
</form>
</div>
</body>
</html>