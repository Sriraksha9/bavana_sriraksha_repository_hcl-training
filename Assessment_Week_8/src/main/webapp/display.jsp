<%@page import="com.bean.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="background-color:aqua;">

<h3 align="center"> List of Books Available </h3>
<a href="index.jsp">GO BACK</a>
<a href="welcome.spring">Login</a><br>
<div align="center">
<table border="1">
	<tr>
			<th>Id</th>
			<th>TITLE</th>
			<th>AUTHOR</th>
			
	</tr>
<% 
	Object obj = session.getAttribute("obj");
	List<Books> listOfBooks = (List<Books>)obj;
	Iterator<Books> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Books book  = ii.next();
		%>
		<tr>
			<td><%=book.getId() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getAuthor() %></td>
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>