create database week11_sriraksha;
use week11_sriraksha;
create table books (
       id integer not null auto_increment,
        name varchar(255),
        genre varchar(255),
        primary key (id)
    ) ;
    
    create table user (
        id integer not null auto_increment,
        userName varchar(255),
        password varchar(255),
        primary key (id)
    ) ; 
    
    create table user_liked_books (
       user_id integer not null,
        book_id integer not null
    ) ;
    
    alter table user_liked_books 
       add constraint 
       foreign key (book_id) 
       references books (id);

    alter table user_liked_books 
       add constraint 
       foreign key (user_id) 
       references user (id);