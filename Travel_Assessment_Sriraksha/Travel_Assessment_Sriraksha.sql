create database travel_sriraksha;

use travel_sriraksha;


create table PASSENGER
 (Passenger_name varchar(20), 
  Category           varchar(20),
  Gender             varchar(20),
  Boarding_City      varchar(20),
  Destination_City   varchar(20),
  Distance           int,
  Bus_Type           varchar(20)
);


create table PRICE
(
	Bus_Type   varchar(20),
	Distance   int,
	Price      int
);


insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;


insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;


/* 1 */

select count(case when gender="M" then 1 end) as Male_Count from passenger where Distance>=600;

select count(case when gender="F" then 1 end) as Female_Count from passenger where Distance>=600;


/* 2 */

select Bus_Type,min(Price) as Minimum_Price from price where Bus_Type="Sleeper";


/* 3 */

select Passenger_name from passenger where Passenger_name like 'S%';


/* 4 */

SELECT Passenger_name , a.Boarding_City, a.Destination_city, a.Bus_Type, b.Price FROM passenger a, price b WHERE a.Distance = b.Distance and a.Bus_type = b.Bus_type;


/* 5 */

SELECT a.Passenger_name, a.Bus_type, b.Price FROM passenger a, price b WHERE a.Distance = 1000 and a.Bus_type = 'Sitting';


/* 6 */

SELECT a.Passenger_name, a.Destination_city as Boardng_city, a.Boarding_city as Destination_city, a.Bus_type, b.Bus_type, b.Price FROM passenger a, price b WHERE Passenger_name = 'Pallavi' and a.Distance = b.Distance;


/* 7 */

SELECT DISTINCT distance FROM passenger ORDER BY Distance desc;


/* 8 */

SELECT Passenger_name, Distance, Distance * 100.0/ (SELECT SUM(Distance) FROM passenger) as Distance_Percentage FROM passenger;


/* 9 */

CREATE VIEW c_view AS SELECT Passenger_name, Category FROM passenger WHERE Category = 'AC';
SELECT * FROM c_view;


/* 10 */

select * from passenger limit 5;

