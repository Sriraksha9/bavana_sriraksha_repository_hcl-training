create database Book_Sriraksha;
use Book_Sriraksha;

create table Books
(
	Title   varchar(20),
	Author   varchar(20),
	Price      float
);

insert into Books values('The 3 mistakes of my life', 'Chetan Bhagat', 600);
insert into Books values('Believe in Yourself', 'Joseph murphy', 500);
insert into Books values('Great Expectations', 'Charles Dickens', 530);
insert into Books values('2 States', 'Chetan Bhagat', 550);
insert into Books values('Tell me a strory', 'ravinder singh', 300);

select * from Books;
