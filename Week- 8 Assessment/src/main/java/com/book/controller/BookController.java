package com.book.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BookController
 */
@WebServlet("/BookController")
public class BookController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		
		PrintWriter pw = response.getWriter();
		
		pw.println("Books");
		pw.println();
		pw.println("Title                         Author                    Price");
		pw.println();
		pw.println("Believe in Yourself           Joseph murphy             500");
		pw.println("Great Expectations            Charles Dickens           530");
		pw.println("2 States                      Chetan Bhagat             550");
		pw.println("Tell me a strory              ravinder singh            300");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		
		PrintWriter pw = response.getWriter();
		
		String user = request.getParameter("user");
		pw.println("Welcome user " + user);
		pw.println();
		
		pw.println("Books");
		pw.println();
		pw.println("Title                         Author                    Price");
		pw.println();
		pw.println("Believe in Yourself           Joseph murphy             500");
		pw.println("Great Expectations            Charles Dickens           530");
		pw.println("2 States                      Chetan Bhagat             550");
		pw.println("Tell me a strory              ravinder singh            300");
	}

}
