package com.hcl.angular.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.angular.model.ERole;
import com.hcl.angular.model.Role;

public interface Rolerepo extends JpaRepository<Role, Long> {

	Optional<Role> findByName(ERole name);
}
