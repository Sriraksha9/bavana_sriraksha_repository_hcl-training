package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.UserOrder;

@Repository

public interface OrderDao extends JpaRepository<UserOrder, Integer>{
	@Query("select o from UserOrder o where o.name=:name")
	public List<UserOrder> getItemsOrderedByName(@Param("name") String name);
	
	@Query("select sum(o.itemPrice) from UserOrder o where o.name=:name")
	public float getTotalByName(@Param("name")String name);
	
	@Query("select sum(o.itemPrice) from UserOrder o")
	public float getTodayEarnings();

}
