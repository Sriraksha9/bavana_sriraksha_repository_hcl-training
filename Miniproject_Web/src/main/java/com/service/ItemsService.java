package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Items;
import com.dao.ItemsDao;

@Service
public class ItemsService {
	@Autowired
	ItemsDao itemsDao;
	
	public String storeMenuItem(Items item) {
		if(itemsDao.existsById(item.getItemId())) {
			return "itemId already exits";
		}else {
			itemsDao.save(item);
			return "items stored sucessfully";
		}
	}
	
	public List<Items> getAllItems(){
		return itemsDao.findAll();
	}
	
	public String deleteItemInfo(int itemId) {
		itemsDao.deleteById(itemId);
		return "product deleted Sucessfully";
	}
	
	public String updateItemPrice(Items itm) {
		Items item=itemsDao.getById(itm.getItemId());
		item.setItemName(itm.getItemName());
		item.setItemType(itm.getItemType());
		item.setItemPrice(itm.getItemPrice());
		itemsDao.saveAndFlush(item);
		return "Item Price Updated Sucessfully";
	}

}
